from setuptools import setup, find_packages

setup(
    name = 'lrpforlstm',
    version = '0.10',
    packages = find_packages(),
    package_data = {
        'lrpforlstm': ['model/*']
    }
)
