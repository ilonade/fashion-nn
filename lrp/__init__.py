from lrpforlstm.LSTM.LSTM_bidi import *

import codecs
import numpy as np

def perform_lrp(sentence, target_class, eps=0.001, bias_factor=0.0):
    words = parse_sentence(sentence)

    net = LSTM_bidi()

    known_words = [w for w in words if w in net.voc]
    word_indices = [net.voc.index(w) for w in known_words]

    Rx, Rx_rev, R_rest = net.lrp(
        word_indices,
        target_class,
        eps,
        bias_factor
    )

    R_words = np.sum(Rx + Rx_rev, axis=1)

    scores = net.s.copy()

    relevancies = []
    for idx, word in enumerate(known_words):
        relevancies.append({
            'word': word,
            'relevancy': R_words[idx]
        })

    return {
        'scores': scores.tolist(),
        'target_class': target_class,
        'relevancies': relevancies
    }

def parse_sentence(sentence):
    result =[]
    words = sentence.split()
    for word in words:
        clean_word = ""
        for char in word:
            if char in '!,.?":;0123456789':
                char = ""
            clean_word += char
        result.append(clean_word)
    return result
