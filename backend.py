# coding: utf-8

from __future__ import print_function
from bottle import route, post, run, template, request, response
from lrp import perform_lrp

@route('/api/classify-article', method=['OPTIONS', 'POST'])
def classify_article():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

    if request.method == 'OPTIONS':
        return {}

    article = request.json
    print(article)
    text = article['text']
    target_class = article['targetClass']

    if target_class == None:
        result = perform_lrp(text, 0)
        return {
            'scores': result['scores'],
            'relevancies': []
        }

    return perform_lrp(text, target_class)

run(host='localhost', port='8080', server='waitress')
