export const t = {
    STORE_CLASSIFICATION: "STORE_CLASSIFICATION",
    UPDATE_ARTICLE_TEXT: "UPDATE_ARTICLE_TEXT",
};

export const actions = {
    storeClassification: payload => ({
        type: t.STORE_CLASSIFICATION,
        payload,
    }),

    updateArticleText: payload => ({
        type: t.UPDATE_ARTICLE_TEXT,
        payload,
    }),
};
