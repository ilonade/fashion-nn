import {combineReducers} from 'redux';
import {t} from '../actions/actions';

const scoresReducer = (state = [], action) => {
    switch (action.type) {
        case t.STORE_CLASSIFICATION:
            return action.payload.scores.map((score, index) => ({
                score,
                targetClass: index,
            })).sort(by('score', -1));

        default:
            return state;
    }
};

const by = (key, dir = 1) => (a, b) => (a[key] - b[key]) * dir;

const relevanciesReducer = (state = [], action) => {
    switch (action.type) {
        case t.STORE_CLASSIFICATION: {
            const relevancies = action.payload.relevancies;
            const normalized = normalize(relevancies.map(r => r.relevancy));
            return relevancies.map((r, index) => ({
                ...r,
                normalizedRelevancy: normalized[index],
            }));
        }

        default:
            return state;
    }
};

const normalize = numbers => {
    const min = Math.min(...numbers);
    const zeroBasedNumbers = numbers.map(x => x - min);
    const max = Math.max(...zeroBasedNumbers);

    if (max === 0) return numbers;

    return zeroBasedNumbers.map(x => x / max);
};

const articleTextReducer = (state = '', action) => {
    switch (action.type) {
        case t.UPDATE_ARTICLE_TEXT:
            return action.payload;

        default:
            return state;
    }
};

export default combineReducers({
    scores: scoresReducer,
    relevancies: relevanciesReducer,
    articleText: articleTextReducer,
});
