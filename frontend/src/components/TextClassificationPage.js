/* eslint-disable react/prop-types */
import React from 'react';
import {connect} from 'react-redux';
import {actions} from '../actions/actions';
import {config} from '../config';
import {Relevancies} from './Relevancies';

const classLabels = {
    0: 'Very Negative',
    1: 'Negative',
    2: 'Neutral',
    3: 'Positive',
    4: 'Very positive',
};

//const highlightExplanationIn = (text, explanation) => {
//    let parts = [text.toLowerCase()];
//
//    const topRelevant =
//        explanation
//            .filter(pair => pair[1] > 0.01)
//            .sort((a, b) => b[1] - a[1]);
//
//    topRelevant.forEach(pair => {
//        const word = pair[0];
//        //const weight = pair[1];
//
//        const nextParts = [];
//
//        parts.forEach(part => {
//            if (typeof part !== 'string' || part.indexOf(word) === -1) {
//                nextParts.push(part);
//            } else {
//                const splitParts = part.split(word);
//                nextParts.push(splitParts[0]);
//                for (let i = 1; i < splitParts.length; i++) {
//                    nextParts.push(<span className="highlight">{word}</span>);
//                    nextParts.push(splitParts[i]);
//                }
//            }
//        });
//
//        parts = nextParts;
//    });
//
//    return parts.map(part =>
//        typeof part === 'string' ?
//            <span>{part}</span> :
//            part);
//};

export const TextClassificationPageComponent = ({
                                                    scores,
                                                    relevancies,
                                                    storeClassification,
                                                    articleText,
                                                    updateArticleText,
                                                }) => {

    const classify = (targetClass = null) => {
        const requestBody = {text: articleText, targetClass};

        return fetch(`${config.apiGateway}/api/classify-article`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: 'POST',
            body: JSON.stringify(requestBody),
        }).then(r => r.json())
            .then(r => storeClassification(r))
            //eslint-disable-next-line no-console
            .catch(e => console.error(e));

    };

    const onTextChange = e => {
        updateArticleText(e.target.value);
    };

    const onExplanationClick = (e, targetClass) => {
        classify(targetClass);
        e.preventDefault();
    };

    let explanationHtml = '';

    if (relevancies.length > 0) {
        explanationHtml = (<div className={`explanation-text-container`}>
            <div className="explanation-text">
                <p className="explanation-title">
                    <span className="relevant">&#9724;</span> The most relevant words
                    <br/>
                    <span className="irrelevant">&#9724;</span> The most irrelevant words
                </p>
                <p>
                    <Relevancies relevancies={relevancies}/>
                </p>
            </div>
        </div>);
    }

    return (<div className="wrapper">
        <div className="bar">
            <p>Text Classification Page</p>
        </div>

        <div className="user-input-container">
            <textarea name="user-input" id="user-input" placeholder="Input text to classify..."
                      onChange={onTextChange} defaultValue={articleText}/>
            <button id="classify" name="classify" type="submit" onClick={() => classify()}>Classify</button>
        </div>

        <div className="classification-data-container titles-container">
            <div className="classification-data titles">
                <p className="col-2">TOP</p>
                <p className="col-2">LABEL</p>
                <p>Score</p>
            </div>
        </div>

        {scores.map((score, index) =>
            (<div key={index} className="classification-data-container">
                <div className="classification-data">
                    <p className="col-2">{index + 1}</p>
                    <p className="col-2">{classLabels[score.targetClass]}</p>
                    <p>{score.score.toFixed(2)}</p>
                    <a id="explanation" href="#"
                       onClick={e => onExplanationClick(e, score.targetClass)}>
                        explain
                    </a>
                </div>
            </div>),
        )}

        {explanationHtml}

        <footer className="footer">
            <p>Enterprise Data Management WS2017/18</p>
        </footer>

    </div>);
};

const mapStateToProps = (state) => {
    return {
        scores: state.scores,
        relevancies: state.relevancies,
        articleText: state.articleText,
        explanationShown: state.explanationShown,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        storeClassification: (predictions) => {
            dispatch(actions.storeClassification(predictions));
        },

        updateArticleText: (text) => {
            dispatch(actions.updateArticleText(text));
        },
    };
};

export const TextClassificationPage = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TextClassificationPageComponent);
