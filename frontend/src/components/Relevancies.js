/* eslint-disable react/prop-types */
import React from 'react';

export const Relevancies = ({relevancies}) => {
    return (
        <span className="relevancies">
            {relevancies.map((r, key) =>
                (<span key={key} style={{
                    backgroundColor: blueToRed(r.normalizedRelevancy * 100),
                    color: whiteOrBlack(r.normalizedRelevancy * 100),
                }}>
                    {r.word + ' '}
                </span>),
            )}
        </span>
    );
};

const blueToRed = percentage => {
    let r, g, b;

    if (percentage < 50) {

        // from blue to white
        r = Math.round(5.1 * percentage);
        g = Math.round(5.1 * percentage);
        b = 255;

    } else {

        // from white to red
        r = 255;
        g = Math.round(510 - 5.10 * percentage);
        b = Math.round(510 - 5.10 * percentage);

    }

    let h = r * 0x10000 + g * 0x100 + b * 0x1;

    return '#' + ('000000' + h.toString(16)).slice(-6);
};

const whiteOrBlack = percentage => {
    if (percentage < 25 || percentage > 75) {
        return '#fff';
    } else {
        return '#000';
    }
};