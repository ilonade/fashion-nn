# coding: utf-8
from __future__ import print_function
import lime
import sklearn
import numpy as np
import sklearn
import sklearn.ensemble
import sklearn.metrics
import sklearn.datasets

# In[126]:


import os
from sklearn.datasets import fetch_20newsgroups
cwd = os.getcwd()
cwd
container_path_test=cwd+'/scikit_learn_data/20news_home/20news-bydate-test'
container_path_train=cwd+'/scikit_learn_data/20news_home/20news-bydate-train'
#categories = ['comp.graphics', 'comp.os.ms-windows.misc']
newsgroups_train = sklearn.datasets.load_files(container_path_train)
newsgroups_test = sklearn.datasets.load_files(container_path_test)
# making class names shorter
class_names = [x.split('.')[-1] if 'misc' not in x else '.'.join(x.split('.')[-2:]) for x in newsgroups_train.target_names]
class_names[3] = 'pc.hardware'
class_names[4] = 'mac.hardware'

inv_class_names = {}
for idx, val in enumerate(class_names):
    inv_class_names[val] = idx


# In[127]:


print(','.join(class_names))
vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(lowercase=False,stop_words='english',decode_error='ignore')

train_vectors = vectorizer.fit_transform(newsgroups_train.data)
test_vectors = vectorizer.transform(newsgroups_test.data)


# In[128]:


from sklearn.naive_bayes import MultinomialNB
nb = MultinomialNB(alpha=.01)
nb.fit(train_vectors, newsgroups_train.target)


# In[129]:


pred = nb.predict(test_vectors)
sklearn.metrics.f1_score(newsgroups_test.target, pred, average='weighted')


# In[130]:


from lime import lime_text
from sklearn.pipeline import make_pipeline
c = make_pipeline(vectorizer, nb)


# In[131]:


from lime.lime_text import LimeTextExplainer
explainer = LimeTextExplainer(class_names=class_names)


def explain(text, label):
    label = inv_class_names[label]
    exp = explainer.explain_instance(text, c.predict_proba, num_features=30, top_labels=4)
    print(exp.available_labels())
    labels = exp.available_labels()
    result = {}
    lst = exp.as_list(label)
    return lst

def classify(text):
    classification = c.predict_proba([text])
    result = {}
    for index, value in enumerate(classification[0]):
        result[class_names[index]] = value
    print(result)
    return result
